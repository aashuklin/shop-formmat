<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="category_wrapper">
    <div class="container category main_section_list">
        <div class="row">
            <h1 style="text-align:center;">Категории товаров</h1>
        </div>
        <div class="row">
            <? foreach($arResult['SECTIONS'] as $section): ?>
			<a href="<?= $section['SECTION_PAGE_URL'] ?>" class="col-xs-12 col-md-3 service">
                    <?if (strlen($section['PICTURE']['SRC'])):?>
                        <div id="category_<?= $section['ID'] ?>" class="single_category" style="background-image:url(<?= $section['PICTURE']['SRC'] ?>)"></div>
                    <?endif;?>
					<div class="single_category_name"><span class='good_title_span'><?= $section['NAME'] ?></span></div>
                </a>
            <? endforeach; ?>
        </div>
    </div>
</div>

<style>
    #category_49{ background-size:contain;}
    #category_50{ background-size:contain;}
    #category_51{background-size:151px 187px;}
    #category_52{background-size:190px 184px;}
</style>



