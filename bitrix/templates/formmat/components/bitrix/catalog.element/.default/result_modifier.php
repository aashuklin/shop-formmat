<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
$arResult['PATH_TO_SERTIFICAT'] = CFile::GetPath(intval($arResult['PROPERTIES']['SERTIFICAT']['VALUE']));

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();