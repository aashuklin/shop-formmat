<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
?>
<div class="container detail_page">
    <a id="nav-toggle-content" href="#"><span></span></a>&nbsp;&nbsp;&nbsp;<span class="small_screen_catalog_heading">Каталог</span>
    <div class="row">
        <div class="col-sm-4 aside">
            <?$APPLICATION->IncludeComponent(
                "1cbit:asidemenu",
                "left_menu_in_catalog",
                Array()
            );?>
        </div>
        <div class="col-sm-8 detail_page_info detailPageInfoSettings">
            <div class="row">
                <div class="col-xs-4 col-sm-4 image">
                    <? if($arResult['DETAIL_PICTURE']['SRC']): ?>
                        <img alt="<?=$arResult['NAME']?>" title="<?=$arResult['NAME']?>" src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" />
                    <? else: ?>
                        <img alt="<?=$arResult['NAME']?>" title="<?=$arResult['NAME']?>" src="/bitrix/templates/formmat/source/images/no_photo.png" />
                    <? endif; ?>
                </div>

                <div class="col-xs-8 col-sm-8 info">
                    <div class="good_name good_info"><div class="col-sm-12"><h1><?= $arResult['NAME'] ?></h1></div></div>
                    <div class="row price good_info">
                        <div class="col-sm-12">
                            <? if($priceRes = CPrice::GetBasePrice(intval($arResult['ID']))): ?>
                                    <?= $priceRes['PRICE']; ?> руб
                            <? else: ?>
                                    стоимость товара не задана
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="good_info">
                        <div class="col-sm-12">
                            <span class="detail_page_icon" id="compare">

                                <span class="action js_add_to_compare" data-id="<?=$arResult["ID"]?>">
                                    <span class="must_be_added <?= !in_array($arResult['ID'],array_keys($_SESSION['COMPARE'][12]['ITEMS'])) ? "active_compare" : ""?>">сравнить</span>
                                    <span class="already_added <?= in_array($arResult['ID'],array_keys($_SESSION['COMPARE'][12]['ITEMS'])) ? "active_compare" : ""?>"><a href="/compare/" data-toggle="tooltip" data-placement="top" title="товар добавлен в сравнения, перейти в сравнения">перейти в сравнения</a></span>
                                </span>&nbsp;
                                <img alt="Сравнение" title="Сравнение" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAYtSURBVGhD7VppaFxVFI77Cu6KCyruBG1t3ptJ1GoUl+L2Q23ENpl7JwlGxb2KFWkZVGgz897LovZHLS4FtRitFlpcEBFcWiwiYmtdCtaKaLRgrAnV1pj6nXPPG2cy82bmzRJTzAeXlzn33HPOd7d37n2pm8L/BZY75+iIo1+yHb3ddtSWiKvukKo9C7ar3464endmsZ34jVI9sYg4agECet521dPo0ZX4ey2e3yGoEQ7O0WPS2ysoyJaBln2o3fn97cdI/bbIothRlqduYyKufo0NTyTqEy37g8AfHFCegikzmiNz1TeWF7+mwYufxzJHfU62LDc20/zWH7DxiUZjSk2D89vR43+j5wejnm6Kdree1JzQB1L9NKftEDvZcTYI3Esk0qQc/fGkIkIAgXY/QCIi4hw0J5r3tRx1F0j/5usXJLK7bq+Zi+ceYS3t2k8ktQUCW+8Hhum0XMSBmJHsOAHkX89PRA3huQFr5dcMm6O8zrDDRVLqBn+dVRWRnpjNzuAY5S843UHbqlQHInCNZBZH/cmE8MyW683wcwuNGBurBuDkGTKOHlvs9zLWwoNSHYgCRDZEvfbpFzhtx7IiIZHYuzEZPxO6CzD6X4ke+VxXaCqXjOm9+nAaAVrojb361GhKXWWC05vJuajlRdm7FuxCL46yVfTHYONVK6UvEo3wsFP6PjKGEVjDAgw1kWAHbmwWywJQ6fbb5M0+CB34KBNhf1wWSnUIUNAyzFFHXytSTDX9gBhdJaK8qMZ7hEaB2mCKDWJmbLQ91SZVpQMGLhcjWzKnEb2labrRwre6YyeLOAfVIALfL0ibYau76zARhwPNSzbi6odFlAaILKc6kHlcRDmolAgnnK7eyW2MrzulqnTwewA9ToYoZxJxGrSTSFA/Uhoj4ixUSgTt5jMBR/8gz41SVTqwuBPc2FUviigHCOhTCfRmEWWhIiKYyujIbw0BdTVsfC+2LhGN4qA0w+8Fci7iHERS+lY27qr3RJSFSohQ0km6lLuZTUcvFF8rRKU4KBXPDCAI9U+1HAqdIQQ2FnXa60WcRiVE0JGrTeCxefS7oaf1eNjZBdnOxr7O41ipGKD8jjFS/DSHYX+CdOkpojTKJTLD6TgFvpFp6x1NXueRIiZfA+zLVY+IKBjogbOoh1GGqcdFHAgaCaOvhsbrl0sEOos4YEc9JyJG1FOXcXu88YtlFWSkxxjRqxtdbZVS0ENfsAOsGTHDKIeIOcjpn0kPJ8qoiA2wVhDXJhNf/HqR5oIXeUZqHb6o9WKKUQ4Ry1VzSAdxfCKiLKDtPeLvDRHlB4ZznTgbJmMlFnrfkPFnxQyjHCIY3fdJBylRp4iyQEks6kd4DfW0nybiXNhu/EJSgvNd0WT8YhEHAvod5BhDvr3JUyeKmBGWiO20nSv6Q1ai62AR5wC+lhmfKimi/ECv8MsQZcT29E0izgan2rF5MDqKwMZwvJ0tNWmEJYK6JVLfJ6K8aEjGG0RvW1BWYWBeQLzoTQP1EV3lWI6+1HLjVyLo+zFqnBUTEUytLmmZhTBEaMdDB/7OneLqc0QcCOiZiw1Xt4ooGLTwECTvIPkKSHyNIANThjBE/r3zUu+KqCCgSwcvxKE+FFFhnNF/9wFRL96CRkvgZA2eq9C4l06KxfbyMEQw3z+jOvhw7VTsiqIF2y/0OTOm6yoxUxuUSsS/3Ci3gHy/mKoNSiVCByZMYX5/YWS24EkpUsEC/bV4GjKp+FwxVRuEmlo4NFEdrclCp04C3XlB72VDBGukWLpSKcIQkfuBlUwGRwh6n0lNFsyp0ejB7i+UYEpV7RCKCEC3JrKhmLnvqDfNbha7jqYPZE+i7bDUDdLdmDStLcISIdC0oRetH3BOoUwbI1LymaQaKIeID35BOjoG3T60eQV/L4u6+qGGlDpdVCYOlRCZVJgiMtkwReS/AG2DlMBRYjm+IGD/snsr//bvphz15XhdvxDZCfsM5wMBzUfJ/tJUhYKtdhMlkuKmtqAztXHM909v4U07UJ2CMw6TUYOlfN6rGEglfmKHSCnyTZFyS8SJaXQOfxEGmcfEXW2Q/o+GGhdkuLX9Dwm6xsTaWFrrQt/rxeUU9lDU1f0DSV4b8EPqLAMAAAAASUVORK5CYII=">
                            </span>
                            <span class="detail_page_icon" id="intolistofgoods">
                                <span class="action" >
<span class="added_to_favorites <?= in_array($arResult['ID'],array_keys($_SESSION['FAVORITES'][12]['ITEMS'])) ? "active_favorite" : ""?>"><a href="/katalog/favorites/">в избранном</a></span>
<span class="js_add_to_favorite <?= !in_array($arResult['ID'],array_keys($_SESSION['FAVORITES'][12]['ITEMS'])) ? "active_favorite" : ""?>" data-id="<?= $arResult['ID'] ?>">в избранное</span>
                                </span>&nbsp;
                                <img alt="Избранное" title="Избранное" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAUISURBVGhD7VpLiB1FFH0mCBriD7+oC1FRjEg+/ZlJjKiJMUYRomQWk5mu6hlhNroR1ERdvGQRnXndPWNExbjwg7gJGHBWfqIJBjfiSlA0EkJABVGGaFQkjsZzqu97vk/3vPcy3c0b44FL171169Y93VX1qmqmlAecir7CCfV2O1D7nUAdMRLqD2hjnbj1MMrlRXagH3dC9RuSPuUE+huUp/F8xw7VYbGdQPkx+kqr3sLKidErkeCHTBZkPuoLtSVVNdAGYgdiH7WfbaQqH6Cj9RgOn9SGRQcCEr/gedK87VOlsxjHHR+6muQotaRRRx/xZZvEeMmiD7mBv87EaQcZ37/izX2Ht7a3ncD3GN+wGUaTni1hDBBjp6mL63eI2YC+pk1cfywpdrPA/3vIidXB8GUSJh1OpO8xwfEUUyqs8bEL7FDP4G19vnxKXyjmGpzQ24j6PyluRd0t5hrYhm0Zg7HEnAqQ2RQT9zaKKR1uoO+jsxV6a8WUiippO/AfFFML+qb0NRRRW8C2Hb845ERf5iimdKQR4ZDjZK0X+D1N31WRf4u4dQ22NUQQqzl+33MPXS5uBvMmgrd2P4eHdNgiVqRcce0abJsUk2KGZF3SGRBRr2FJ/R2Bx+rFCtSjmNB/oe5LjPU9pyNsC5llrOb4pk/0LWlkQCRUbyHh46I2wI7UMDr8Gh3PnI4g7ldOxd8q4RrAPtm3qPkSyRO5EumP1FVWqAZNBbAyGr7eDfVmUbkorLBCf4OoJauib6WIimT8DW40slxU+KsHVlXUdaKiXg1WfzxzJYKkn8CP0t/LXhhYauoC9TyHCMsEyvsw9o+IWoLvQYqo0LkL0PtEpf8MY7DMmIyNvnZSz5UI6rabYLuHzhf9xWodgfI0EjsqKtt+TBGVxI/SR1STLGOwzJiMDTK7qnW5EbEnRm9EMk+aCoBbDaw4j4haciN1pxN4WlR+wc0NQw91VqDvELXEtvVbG8ZmHyznSqRI/E+EOCOJOJPqZqw6DzfvibJCIUTMUgkbffH8VMymPSZs4nminWBp3iZhDAoh4jzjXYyOTxoiOCCJOV6FGk513Yh+U8IYFEKEiHfF6hVuucWUKQojkjeK+yKY6BgS71uRf6+YMkUhRHgBEO+LzPXPt2Lm3moHbV3Iq9K0BYUQub2szyEB+uLLvCdmbFF0P/TxjiVQm6RpCwohQqzYPXIph1V/tOVcMWWKwogQVnlsiRQzRzFEyuVF8OPFNMa5qog1/kqB2uJG/kCnwq9q7Rk7W0LUUAgRnhRjEmaO/CBm/Lbol6r2bqT+VFlFMV+Ed7jxtuIP1D8l1urhaL1d8e7qVJyKtzrpdr4YIgWgMCK8CoXv0Npnt14kpkxRCBGuVvjx+5G+eB4SM9r76zDcmi7i9Mv1Nyedohgi4eAlmNiz9MVcqV028JYFk7/5Iu4n+A2JSyLWTIyexy9bL4j/c+5ECCbHCe9O+LeJqXvEf657l30my79Le25EskDflHeTJDyNL2e2LSi/LbbjjZd3PUyk9mcFuf91Qu+GeA/H+eGvMU6CBUNkLhLEwiCC7f9cJIiFQoR3vqkkiPkTCXQEmcVE/Cx7UV8YIm1IEPMmwtMgOn0ddu52Mxe8pDfcYGSZdJeKeRPpFZyZRDhO6YzJt8ueHLm2l4Q5mdy47W+Hgb0Di9HgoGnQk6IOMEdJd27w2Gn+IyFQ23pJmFPSkfg/hFLpHxg35KPJz/VUAAAAAElFTkSuQmCC">
                            </span>
                            <?if(strlen($arResult['PATH_TO_SERTIFICAT'])>0):?>
                                <a class="link_to_sertificat" title="скачать" href="<?=$arResult['PATH_TO_SERTIFICAT']?>" target="_blank">сертификат</a>
                            <?endif?>
                        </div>
                    </div>
                    <div class="good_info">
                        <div class="col-sm-12 counter_area">
                            <span class="counter minus"> - </span>

                            <?= $item['PROPERTIES']['CML2_BASE_UNIT']['VALUE']?>
                            <span class="counter digit">
                                <span class="for_count">
                                    <input type="text" value="1 <?= $arResult['PROPERTIES']['CML2_BASE_UNIT']['VALUE']?>" data-unit="<?= $arResult['PROPERTIES']['CML2_BASE_UNIT']['VALUE']?>" />
                                </span>
                            </span>

                            <span  class="counter plus"> + </span>
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-sm-12 into_basket" data-id="<?= $arResult['ID'] ?>" data-picture='<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>'>
                            <button onclick="yaCounter50747392.reachGoal('dobavit_korzina'); return true;">В КОРЗИНУ</button>
                        </div>
                        <?if(intval($arResult['PROPERTIES']['FORCALCULATOR']['VALUE'])>0):?>
                            <div class="col-sm-12 forcalculator">
                                <a target="_blank" href="/stroitelnyy-kalkulyator?countcalculator=y&id=<?=$arResult['PROPERTIES']['FORCALCULATOR']['VALUE']?>">
                                    Рассчитать расход
                                </a>
                            </div>
                        <?endif?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="characteristicks_in_pictures">
                        <?foreach($arResult['PROPERTIES']['CHARACTERISTIKSINPICTURES']['VALUE'] as $value):?>
                            <?php
                            $arrayOfCurrentFile = CFile::GetFileArray(intval($value));
                            $original_name = explode(".",$arrayOfCurrentFile['ORIGINAL_NAME'])[0];
                            $original_path = CFile::GetPath(intval(intval($value)));
                            ?>
                            <img width='45' data-toggle="tooltip" class='characteristik' src="<?=$original_path?>" alt="<?=$original_name?>" title="<?=$original_name?>"/>
                        <?endforeach?>
                    </div>
                </div>
            </div>
            <? if(!empty($arResult['PREVIEW_TEXT'])): ?>
                <div class="row additional_description">
                    <div class="col-sm-12">
                        <div>
                            <?= $arResult['PREVIEW_TEXT'] ?>
                            <br />
                            <?= $arResult['DETAIL_TEXT'] ?>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?=($arResult['PROPERTIES']['ADDITIONALDESCRIPTION']['~VALUE']['TEXT'])?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; ?>
            <br/>
            <?if(!empty($arResult['PROPERTIES']['USINGTECHNOLOGY']['VALUE'])):?>
                <?=$arResult['PROPERTIES']['USINGTECHNOLOGY']['~VALUE']['TEXT']?>
            <?endif?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.products.viewed",
                "viewed_elements_on_detail_page",
                Array(
                    "ACTION_VARIABLE" => "action_cpv",
                    "ADDITIONAL_PICT_PROP_12" => "-",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "BASKET_URL" => "/personal/basket.php",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "CART_PROPERTIES_12" => array(","),
                    "CONVERT_CURRENCY" => "N",
                    "DEPTH" => "2",
                    "DISPLAY_COMPARE" => "N",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => "12",
                    "IBLOCK_MODE" => "multi",
                    "IBLOCK_TYPE" => "1c_catalog",
                    "LABEL_PROP_12" => array(""),
                    "LABEL_PROP_MOBILE_12" => array(),
                    "LABEL_PROP_POSITION" => "top-left",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "PAGE_ELEMENT_COUNT" => "3",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => array("Интернет-магазин"),
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "PROPERTY_CODE_12" => array(","),
                    "PROPERTY_CODE_MOBILE_12" => array(""),
                    "SECTION_CODE" => "",
                    "SECTION_ELEMENT_CODE" => "",
                    "SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
                    "SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_FROM_SECTION" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_PRODUCTS_12" => "N",
                    "SHOW_SLIDER" => "Y",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "N"
                )
            );?>

        </div>
    </div>



</div>



<script type="text/javascript">
    var viewedCounter = {
        path: '/bitrix/components/bitrix/catalog.element/ajax.php',
        params: {
            AJAX: 'Y',
            SITE_ID: "<?= SITE_ID ?>",
            PRODUCT_ID: "<?= $arResult['ID'] ?>",
            PARENT_ID: "<?= $arResult['ID'] ?>"
        }
    };
    BX.ready(
        BX.defer(function(){
            BX.ajax.post(
                viewedCounter.path,
                viewedCounter.params
            );
        })
    );
</script>
