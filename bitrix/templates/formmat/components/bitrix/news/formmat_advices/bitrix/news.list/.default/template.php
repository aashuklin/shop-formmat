<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 advise">
            <h1>ФОРММАТ советует</h1>
            <br/>
            <?foreach($arResult['ITEMS'] as $item):?>
                <h3><?=$item['NAME']?></h3>
                <div class="single_advise" href="<?=$item['DETAIL_PAGE_URL']?>">
                    <p><?=$item['PREVIEW_TEXT']?></p>
                    <a class="single_advise_link pull-right" href="<?=$item['DETAIL_PAGE_URL']?>">ЧИТАТЬ &nbsp; <img class='link_image' src="/bitrix/templates/formmat/source/images/link_arrow.png" /></a>
                </div>
                <br/>
            <?endforeach?>
        </div>
    </div>
</div>
