<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="feedbacks_wrapper">
    <div class="container feedbacks custom_container">
        <div class="row">
            <h1>ОТЗЫВЫ</h1>
        </div>
        <div class="row">
            <div class="all_feedbacks">
                <? foreach($arResult['ITEMS'] as $arItem): ?>
                    <?php
                    //получаем ссылки для редактирования и удаления элемента
                    $arButtons = CIBlock::GetPanelButtons(
                        $arItem["IBLOCK_ID"],
                        $arItem["ID"],
                        0,
                        array("SECTION_BUTTONS"=>false, "SESSID"=>false)
                    );
                    $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
                    $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
                    //добавляем действия (экшены) для управления элементом
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => 'Подтвердить'));
                    ?>
                    <div class="single_feedback" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <? if($arItem['DETAIL_PICTURE']): ?>
                            <div class="client_image">
                                <img src='<?= $arItem['DETAIL_PICTURE']['SRC'] ?>' />
                            </div>
                        <? endif; ?>
                        <br/>
                        <div class="client_name">
                            <?= $arItem['NAME']?>
                        </div>
                        <div class="client_position">
                            <?= $arItem['PREVIEW_TEXT'] ?>
                        </div>
                        <br/>
                        <div class='client_feedback'>
                            <?= $arItem['DETAIL_TEXT'] ?>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
