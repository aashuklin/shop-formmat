<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); use Bitrix\Main\Page\Asset; ?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5SVQ7F2');</script>
        <!-- End Google Tag Manager -->
        <title><?$APPLICATION->ShowTitle();?></title>
        <?$APPLICATION->ShowHead();?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?$APPLICATION->IncludeFile('/include/favicon.php',array(),array())?>
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

    </head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SVQ7F2"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132494070-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-132494070-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<?$APPLICATION->IncludeFile('/bitrix/templates/formmat/include/analitics.php')?>

    <div id="panel">
        <?$APPLICATION->ShowPanel();?>
    </div>

    <div class="small_screen_toggle">
        <div class="container">
            <div class="row">
                <div class="col-xs-2"><a id="nav-toggle" href="#"><span></span></a></div>
                <div class="col-xs-10 geo">
                    <span class="map_marker"><svg aria-hidden="true" data-prefix="fas" data-icon="map-marker-alt" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path></svg></span>
                    Воронеж
                    | <a href="/kontakty/" class="where">Где купить?</a>
                </div>
            </div>
        </div>
        <div class="small_screen_menu">
            <div class="row">
                <div class="col-xs-12">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "smallscreenmenu", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                        "COMPONENT_TEMPLATE" => ".default",
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-2 left_logo">
                    <div class="logo_area_wrapper">
                        <a class='logo' href='/'>
                            <img src="/bitrix/templates/formmat/images/formmat_logo.png" alt="Формматериалы" title="Формматериалы" />
                        </a>
                    </div>
                    <div class="small_screen_top_search">
                        <?$APPLICATION->IncludeComponent("bitrix:search.title", "top_search", Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "NUM_CATEGORIES" => "1",	// Количество категорий поиска
                            "TOP_COUNT" => "5",	// Количество результатов в каждой категории
                            "ORDER" => "date",	// Сортировка результатов
                            "USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
                            "CHECK_DATES" => "N",	// Искать только в активных по дате документах
                            "SHOW_OTHERS" => "N",	// Показывать категорию "прочее"
                            "PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
                            "SHOW_INPUT" => "Y",	// Показывать форму ввода поискового запроса
                            "INPUT_ID" => "title-search-input",	// ID строки ввода поискового запроса
                            "CONTAINER_ID" => "title-search-mobile",	// ID контейнера, по ширине которого будут выводиться результаты
                            "CATEGORY_0_TITLE" => "",	// Название категории
                            "CATEGORY_0" => "",	// Ограничение области поиска
                        ),
                            false
                        );?>
                    </div>
                    <div class="top_icons">
                        <div class="col-xs-4 single_icon">
                            <a href="/katalog/favorites/"><img src="<?= SITE_TEMPLATE_PATH ?>/source/images/small_screen_list.png" alt="список" title="список" class="favorites_katalog_icon" /></a>
                        </div>
                        <div class="col-xs-4 single_icon">
                            <a href="/personal/basket/"><img src="<?= SITE_TEMPLATE_PATH ?>/source/images/small_screen_basket.png" alt="корзина" title="корзина" class="personal_katalog_icon" /></a>
                        </div>
                        <div class="col-xs-4 single_icon">
                            <a href="/personal/data/"><img src="<?= SITE_TEMPLATE_PATH ?>/source/images/small_screen_lk.png" alt="личный кабинет" title="личный кабинет" class="favorites_katalog_icon" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 header_basket">
                    <ul class="nav nav-justified links top_links">
                        <li class='third'>
                            <? if(!$USER->IsAuthorized()): ?>
                                <span><a href="/personal/registration.php">Регистрация</a></span>
                            <? else: ?>
                                <span><a href="/personal/data/" class="user_login"><?= $USER->GetLogin()?></a></span>
                            <? endif; ?>
                            <? if(!$USER->IsAuthorized()): ?>
                                | <span><a href="#" id="enter_form_second" class="enter_form">Вход</a></span>
                            <? else: ?>
                                | <span><a href="<?=$APPLICATION->GetCurPageParam("logout=yes", array("login", "logout", "register", "forgot_password", "change_password"));?>">Выход</a></span>
                            <? endif; ?>
                        </li>
                        <li class='first'><span class="location"><!--<span class="glyphicon glyphicon-map-marker"></span>-->&nbsp;
                                <i class="fas fa-map-marker-alt"></i>&nbsp;Воронеж
                                </span> <span class="border_thing">|</span> <span><a href="/kontakty/">Где купить?</a></span></li>
                        <li class='second second_li_glyphicon'><!--<span class="glyphicon glyphicon-list-alt"></span>-->&nbsp;
                            <a href="/katalog/favorites/" class="list_of_goods list_of_goods_settings"><i class="fas fa-list-ul"></i>&nbsp;Избранное</a>
                            &nbsp;
                            <!--<span class="glyphicon glyphicon-shopping-cart"></span>-->&nbsp;

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket.line",
                                "formmaterial",
                                array(
                                    "PATH_TO_BASKET" => SITE_DIR."personal/basket/",
                                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                    "SHOW_PERSONAL_LINK" => "N",
                                    "COMPONENT_TEMPLATE" => "formmaterial",
                                    "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_TOTAL_PRICE" => "Y",
                                    "SHOW_EMPTY_VALUES" => "Y",
                                    "SHOW_AUTHOR" => "N",
                                    "PATH_TO_AUTHORIZE" => "",
                                    "SHOW_REGISTRATION" => "N",
                                    "PATH_TO_REGISTER" => SITE_DIR."login/",
                                    "PATH_TO_PROFILE" => SITE_DIR."personal/",
                                    "SHOW_PRODUCTS" => "N",
                                    "POSITION_FIXED" => "N",
                                    "HIDE_ON_BASKET_PAGES" => "N"
                                ),
                                false,
                                array(
                                    "0" => ""
                                )
                            );
                            ?>
                        </li>
                    </ul>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:search.title",
                        "top_search",
                        array(
                            "COMPONENT_TEMPLATE" => "top_search",
                            "NUM_CATEGORIES" => "10",
                            "TOP_COUNT" => "5",
                            "ORDER" => "date",
                            "USE_LANGUAGE_GUESS" => "Y",
                            "CHECK_DATES" => "N",
                            "SHOW_OTHERS" => "N",
                            "PAGE" => "#SITE_DIR#search/index.php",
                            "SHOW_INPUT" => "Y",
                            "INPUT_ID" => "title-search-input-second",
                            "CONTAINER_ID" => "title-search",
                            "CATEGORY_0_TITLE" => "",
                            "CATEGORY_0" => array(
                            ),
                            "CATEGORY_1_TITLE" => "",
                            "CATEGORY_1" => array(
                            ),
                            "CATEGORY_2_TITLE" => "",
                            "CATEGORY_2" => array(
                            ),
                            "CATEGORY_3_TITLE" => "",
                            "CATEGORY_3" => array(
                            ),
                            "CATEGORY_4_TITLE" => "",
                            "CATEGORY_4" => array(
                            ),
                            "CATEGORY_5_TITLE" => "",
                            "CATEGORY_5" => array(
                            ),
                            "CATEGORY_6_TITLE" => "",
                            "CATEGORY_6" => array(
                            ),
                            "CATEGORY_7_TITLE" => "",
                            "CATEGORY_7" => array(
                            ),
                            "CATEGORY_8_TITLE" => "",
                            "CATEGORY_8" => array(
                            ),
                            "CATEGORY_9_TITLE" => "",
                            "CATEGORY_9" => array(
                            )
                        ),
                        false
                    );?>
                </div>
                <div class="col-sm-2 work_time_parent">
                    <div class="work_time">
                        <div class="work_time_wrapper">
                            <div>Пн-Пт <b>8:00 - 18:00</b></div>
                            <div>Cб <b><span>Доставка  10:00-16:00  </span></b></div>
                            <div>Вс - выходной</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 registration_hidden">
                    <ul class="nav nav-justified links">
                        <li class='third'>
                            <? if(!$USER->IsAuthorized()): ?>
                                <span><a href="/personal/registration.php">Регистрация</a></span>
                            <? else: ?>
                                <div class="admin_popup_parent">
                                    <div class="admin_popup_arrow" id="to_be_close"></div>
                                    <div class="admin_popup">
                                        <ul class='this_is_admin_list'>
                                            <li class="admin_popup_list"><a href="/personal/order/"><span>ИСТОРИЯ ПОКУПОК </span></a></li>
                                            <li class="admin_popup_list"><a href="/personal/data/"><span>ЛИЧНЫЕ ДАННЫЕ </span></a></li>
                                            <li class="admin_popup_list"><a href="/personal/delivery_address/"><span>АДРЕС ДОСТАВКИ </span></a></li>
                                            <li class="admin_popup_list"><a href="/personal/change_password/"><span>СМЕНА ПАРОЛЯ </span></a></li>
                                            <li class="admin_popup_list"><a href="/personal/change_email/"><span>СМЕНА E-MAIL </span></a></li>
                                        </ul>
                                    </div>
                                    <a href="/personal/data/" class="user_login"><?= $USER->GetLogin()?></a>
                                </div>
                            <? endif; ?>
                            <? global $USER; if(!$USER->IsAuthorized()): ?>
                                | <span><a href="#" class="enter_form">Вход</a></span>
                            <? else: ?>
                                | <span><a href="<?=$APPLICATION->GetCurPageParam("logout=yes", array("login", "logout", "register", "forgot_password", "change_password"));?>">Выход</a></span>
                            <? endif; ?>
                        </li>
                    </ul>
                    <div class="company_phone"><img alt="Телефон компании" title="Телефон компании" src="/bitrix/templates/formmat/images/phone_icon.png" />
                        <a href="tel:74732202820" class="callibri_phone single_mobile_phone first_phone">+ 7 (473) 220-28-20</a>
                    </div>
                    <div class="company_phone"><img alt="Телефон компании" title="Телефон компании" src="/bitrix/templates/formmat/images/phone_icon.png" />
                        <a href="tel:+79304192222" class="callibri_phone single_mobile_phone second_phone">+ 7 (930) 419-22-22</a>
                    </div>
                    <div class="order_phone"><span>заказать звонок</span></div>
                </div>
            </div>
            <div class="row menu_area">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "mainmenu", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                        "COMPONENT_TEMPLATE" => ".default",
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </header>
<?if($APPLICATION->GetCurPage(false)!=="/"):?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "just_to_take_info",
                    Array()
                );?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="action_wrapper">
                    <b>c 1 июля</b>
                    <div> - при оплате Сухих Строительных Смесей (кроме Кладочных смесей и Пескобетонов) в нашем интернет магазине через сбербанк, дополнительная скидка 5% (при оформлении заказа выберите оплату через сбербанк и получите экономию)</div>
                    <!--<div> - у вас есть купон? Получите 10% скидку прописав номер промокода при выборе способа оплаты</div>-->
                </div>
                <style>
                    .action_wrapper{background-image:url(/bitrix/templates/formmat/images/salesber.png); color:#000; font-weight:bold; text-align:left; margin-bottom:5px; padding:15px 15px 15px 30px; font-weight:normal;}
                </style>
            </div>
        </div>
    </div>
<?endif?>